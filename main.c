#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#include "kvlist_cstring_int.h"

int main()
{
    kvlist_cstring_int_t *kvlist = kvlist_cstring_int_new();
    kvlist_cstring_int_put(kvlist, "heh", 1);
    kvlist_cstring_int_put(kvlist, "mde", 5);
    kvlist_cstring_int_put(kvlist, "heh", 8);

    assert(kvlist_cstring_int_get(kvlist, "heh"));
    assert(kvlist_cstring_int_get(kvlist, "mde"));

    printf(
            "%d\n",
            *kvlist_cstring_int_get(kvlist, "heh")
            + *kvlist_cstring_int_get(kvlist, "mde"));

    kvlist_cstring_int_rm(kvlist, "ss");
    kvlist_cstring_int_rm(kvlist, "md");

    assert(kvlist_cstring_int_get(kvlist, "mde"));

    kvlist_cstring_int_rm(kvlist, "heh");
    kvlist_cstring_int_rm(kvlist, "mde");
    kvlist_cstring_int_destroy(kvlist);
}
