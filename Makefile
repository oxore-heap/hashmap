SOURCES+=kvlist_cstring_int.c
SOURCES+=kvlist.c
SOURCES+=hashmap.c
OBJECTS=$(SOURCES:.c=.o)

COMMON_FLAGS+=-fsanitize=address

CFLAGS+=-Wall
CFLAGS+=-Wextra
CFLAGS+=-Wpedantic
CFLAGS+=-g3
CFLAGS+=$(COMMON_FLAGS)

LDFLAGS+=$(COMMON_FLAGS)

all: main
all: kvlist-test
all: hashmap-test

main: $(OBJECTS) main.o
	$(CC) $(LDFLAGS) -o $@ $^

kvlist-test: $(OBJECTS) kvlist-test.o
	$(CC) $(LDFLAGS) -o $@ $^

hashmap-test: $(OBJECTS) hashmap-test.o
	$(CC) $(LDFLAGS) -o $@ $^

clean:
	rm -rfv $(OBJECTS) kvlist-test main hashmap-test
