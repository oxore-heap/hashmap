#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "hashmap.h"

static void test_put_get_same_value()
{
    hashmap_t *hashmap = hashmap_new();
    assert(hashmap);

    int data = 123;
    hashmap_put(hashmap, "heh", strlen("heh") + 1, &data, sizeof(data));

    size_t vsz = 0;
    const int *valueptr = hashmap_get(hashmap, "heh", strlen("heh") + 1, &vsz);
    assert(valueptr != NULL);
    assert(vsz == sizeof(data));
    assert(*valueptr == 123);

    hashmap_destroy(hashmap);
}

static void test_get_value_that_doesnt_exist_returns_null()
{
    hashmap_t *hashmap = hashmap_new();
    assert(hashmap);

    assert(hashmap_get(hashmap, "heh", strlen("heh") + 1, NULL) == NULL);

    hashmap_destroy(hashmap);
}

static void test_get_after_rm_returns_null()
{
    hashmap_t *hashmap = hashmap_new();

    int data = 1;
    assert(hashmap_put(hashmap, "mde", strlen("mde") + 1, &data, sizeof(data)));

    hashmap_rm(hashmap, "mde", strlen("mde") + 1);
    assert(hashmap_get(hashmap, "mde", strlen("mde") + 1, NULL) == NULL);

    hashmap_destroy(hashmap);
}

int main()
{
    test_put_get_same_value();
    test_get_value_that_doesnt_exist_returns_null();
    test_get_after_rm_returns_null();
}
