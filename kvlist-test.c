#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "kvlist_cstring_int.h"
#include "kvlist.h"

void test_kvlist_cstring_int(void)
{
    kvlist_cstring_int_t *kvlist = kvlist_cstring_int_new();

    kvlist_cstring_int_put(kvlist, "heh", 1);
    kvlist_cstring_int_put(kvlist, "mde", 5);
    kvlist_cstring_int_put(kvlist, "heh", 8);

    assert(kvlist_cstring_int_get(kvlist, "heh"));
    assert(kvlist_cstring_int_get(kvlist, "mde"));
    assert(
            *kvlist_cstring_int_get(kvlist, "heh")
            + *kvlist_cstring_int_get(kvlist, "mde") == 8 + 5);

    kvlist_cstring_int_rm(kvlist, "ss");
    kvlist_cstring_int_rm(kvlist, "md");

    assert(kvlist_cstring_int_get(kvlist, "mde"));

    kvlist_cstring_int_rm(kvlist, "heh");
    kvlist_cstring_int_rm(kvlist, "mde");
    kvlist_cstring_int_destroy(kvlist);
}

void test_kvlist(void)
{
    kvlist_t *kvlist = kvlist_new();
    int i;

    i = 1;
    assert(kvlist_put(kvlist, "heh", strlen("heh") + 1, &i, sizeof(int)));
    i = 5;
    assert(kvlist_put(kvlist, "mde", strlen("mde") + 1, &i, sizeof(int)));
    i = 8;
    assert(kvlist_put(kvlist, "heh", strlen("heh") + 1, &i, sizeof(int)));

    size_t size;
    assert(kvlist_get(kvlist, "heh", strlen("heh") + 1, &size));
    assert(kvlist_get(kvlist, "mde", strlen("mde") + 1, &size));
    assert(
            *(const int *)kvlist_get(kvlist, "heh", strlen("heh") + 1, &size)
            + *(const int *)kvlist_get(kvlist, "mde", strlen("mde") + 1, &size)
            == 8 + 5);

    kvlist_rm(kvlist, "ss", strlen("ss") + 1);
    kvlist_rm(kvlist, "md", strlen("md") + 1);

    assert(kvlist_get(kvlist, "mde", strlen("mde") + 1, NULL));

    kvlist_rm(kvlist, "heh", strlen("heh") + 1);
    kvlist_rm(kvlist, "mde", strlen("mde") + 1);
    kvlist_destroy(kvlist);
}

int main()
{
    test_kvlist_cstring_int();
    test_kvlist();
}
