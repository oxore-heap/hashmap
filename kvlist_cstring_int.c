#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "kvlist.h"
#include "kvlist_cstring_int.h"

bool kvlist_cstring_int_is_empty(struct kvlist *kvlist)
{
    return kvlist_is_empty(kvlist);
}

const int *kvlist_cstring_int_get(struct kvlist *kvlist, const char *key)
{
    return kvlist_get(kvlist, key, strlen(key) + 1, NULL);
}

void kvlist_cstring_int_rm(struct kvlist *kvlist, const char *key)
{
    kvlist_rm(kvlist, key, strlen(key) + 1);
}

bool kvlist_cstring_int_put(struct kvlist *kvlist, const char *key, int value)
{
    return kvlist_put(kvlist, key, strlen(key) + 1, &value, sizeof(value));
}

struct kvlist *kvlist_cstring_int_new()
{
    return kvlist_new();
}

void kvlist_cstring_int_destroy(struct kvlist *kvlist)
{
    kvlist_destroy(kvlist);
}
