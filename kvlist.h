#pragma once

struct kvlist;

typedef struct kvlist kvlist_t;

kvlist_t       *kvlist_new();
bool            kvlist_is_empty(kvlist_t *kvlist);
const void     *kvlist_get(kvlist_t *kvlist, const void *key, size_t ksz, size_t *vsz);
void            kvlist_rm(kvlist_t *kvlist, const void *key, size_t ksz);
bool            kvlist_put(kvlist_t *kvlist, const void *key, size_t ksz, const void *value, size_t vsz);
void            kvlist_destroy(kvlist_t *kvlist);
size_t          kvlist_snprintf(char *buffer, size_t n, kvlist_t *kvlist);
