#pragma once

#include "kvlist.h"

struct hashmap;

typedef struct hashmap hashmap_t;

hashmap_t  *hashmap_new();
const void *hashmap_get(const hashmap_t *hashmap, const void *key, size_t ksz, size_t *vszp);
void        hashmap_rm(hashmap_t *hashmap, const void *key, size_t ksz);
bool        hashmap_put(hashmap_t *hashmap, const void *key, size_t ksz, const void* val, size_t vsz);
void        hashmap_destroy(hashmap_t *hashmap);
void        hashmap_set_manual_resize(hashmap_t *hashmap, bool manual);
bool        hashmap_resize(hashmap_t *hashmap);
float       hashmap_get_fill(const hashmap_t *hashmap);
size_t      hashmap_snprintf(void *buffer, size_t n, const hashmap_t *hashmap);
