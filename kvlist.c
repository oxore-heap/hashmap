#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "kvlist.h"

struct kv {
    struct kv *next;
    void *key;
    void *value;
    size_t ksz;
    size_t vsz;
};

struct kvlist {
    struct kv *first;
};

static inline struct kv *get(struct kv *kv, const void *key, size_t ksz)
{
    while (kv) {
        if (ksz == kv->ksz) {
            if (!memcmp(kv->key, key, ksz)) {
                return kv;
            }
        }

        kv = kv->next;
    }

    return NULL;
}

bool kvlist_is_empty(struct kvlist *kvlist)
{
    if (kvlist == NULL)
        return true;

    return !kvlist->first;
}

const void *kvlist_get(
        struct kvlist *kvlist,
        const void *key,
        size_t ksz,
        size_t *vsz)
{
    if (kvlist == NULL)
        return NULL;

    struct kv *kv = get(kvlist->first, key, ksz);

    if (kv) {
        if (vsz) {
            *vsz = kv->vsz;
        }
        return kv->value;
    }

    return NULL;
}

void kvlist_rm(struct kvlist *kvlist, const void *key, size_t ksz)
{
    if (kvlist == NULL)
        return;

    struct kv *current = kvlist->first;
    struct kv *prev = current;

    while (current) {
        if (ksz == current->ksz) {
            if (!memcmp(current->key, key, ksz)) {
                if (current == kvlist->first) {
                    kvlist->first = kvlist->first->next;
                } else {
                    prev->next = current->next;
                }

                free(current->key);
                free(current->value);
                free(current);
                return;
            }
        }

        prev = current;
        current = current->next;
    }
}

bool kvlist_put(
        struct kvlist *kvlist,
        const void *key,
        size_t ksz,
        const void *value,
        size_t vsz)
{
    if (kvlist == NULL)
        return false;

    struct kv *current = kvlist->first;

    /* Rewrite if key already exists */

    if (current) {
        current = get(current, key, ksz);
        if (current) {
            void *tmp_value = calloc(1, vsz);
            if (tmp_value == NULL) {
                return false;
            }

            memcpy(tmp_value, value, vsz);
            free(current->value);
            current->value = tmp_value;

            return true;
        }
    }

    /* Allocate and fill new key-value pair */

    current = calloc(1, sizeof(struct kv));
    if (current == NULL)
        return false;

    current->key = calloc(1, ksz);
    if (current->key == NULL) {
        free(current);
        return false;
    }

    current->value = calloc(1, vsz);
    if (current->value == NULL) {
        free(current->key);
        free(current);
        return false;
    }

    current->ksz = ksz;
    memcpy(current->key, key, ksz);
    current->vsz = vsz;
    memcpy(current->value, value, vsz);

    /* Insert as first element */

    if (kvlist->first) {
        current->next = kvlist->first;
        kvlist->first = current;
    } else {
        kvlist->first = current;
    }

    return true;
}

struct kvlist *kvlist_new()
{
    return calloc(1, sizeof(struct kvlist));
}

void kvlist_destroy(struct kvlist *kvlist)
{
    if (kvlist == NULL)
        return;

    struct kv *current = kvlist->first;
    struct kv *prev = current;

    while (current) {
        prev = current;
        current = current->next;
        free(prev->key);
        free(prev->value);
        free(prev);
    }

    free(kvlist);
}
