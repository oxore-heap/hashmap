#pragma once

struct kvlist;

typedef struct kvlist kvlist_cstring_int_t;

kvlist_cstring_int_t *kvlist_cstring_int_new();
bool        kvlist_cstring_int_is_empty(kvlist_cstring_int_t *kvlist);
const int  *kvlist_cstring_int_get(kvlist_cstring_int_t *kvlist, const char *key);
void        kvlist_cstring_int_rm(kvlist_cstring_int_t *kvlist, const char *key);
bool        kvlist_cstring_int_put(kvlist_cstring_int_t *kvlist, const char *key, int value);
void        kvlist_cstring_int_destroy(kvlist_cstring_int_t *kvlist);
size_t      kvlist_cstring_int_snprintf(char *buffer, size_t n, kvlist_cstring_int_t *kvlist);
