#pragma once

#include "kvlist.h"

struct hashmap;

typedef struct hashmap hashmap_cstring_int_t;

hashmap_cstring_int_t  *hashmap_new(bool bucket_based);
void       *hashmap_get(const hashmap_cstring_int_t *hashmap, const void *key, size_t ksz, size_t *vsz);
void        hashmap_rm(hashmap_cstring_int_t *hashmap, const void *key, size_t ksz);
bool        hashmap_put(hashmap_cstring_int_t *hashmap, const void *key, size_t ksz, const void *val, size_t vsz);
void        hashmap_destroy(hashmap_cstring_int_t *hashmap);
void        hashmap_set_manual_resize(hashmap_cstring_int_t *hashmap, bool manual);
bool        hashmap_resize(hashmap_cstring_int_t *hashmap);
float       hashmap_get_fill(const hashmap_cstring_int_t *hashmap);
size_t      hashmap_snprintf(char *buffer, size_t n, const hashmap_cstring_int_t *hashmap);
